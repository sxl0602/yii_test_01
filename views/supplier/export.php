<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SupplierExportForm;

/* @var $this yii\web\View */
/* @var $model app\models\SupplierExportForm */
/* @var $form ActiveForm */
$this->title = '导出';
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div id="supplier-export" class="supplier-export">
    <?php $form = ActiveForm::begin(); ?>
        <?php echo $form->field($model, 'columns')->checkboxList(SupplierExportForm::COLUMNS_DEFAULT); ?>

        <?php
            /**
            echo $form->beginField($model, 'columns');
            echo \yii\bootstrap4\Html::label('请选择要导出的字段', 'columns');
            echo \yii\bootstrap4\Html::checkboxList('columns', SupplierExportForm::COLUMNS_DEFAULT, SupplierExportForm::COLUMNS_DEFAULT);
            var_dump($model->hasErrors('columns'));
            if ($model->hasErrors('columns')) {
                echo \yii\bootstrap4\Html::error($model, 'columns');
            }
            echo $form->endField();
             **/
        ?>
    
        <div class="form-group">
            <?= Html::submitButton('导出', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- supplier-export -->
