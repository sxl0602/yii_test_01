<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SupplierExportForm extends Model
{
    const COLUMNS_DEFAULT = [
        'id' => 'id',
        'name' => 'name',
        'code' => 'code',
        't_status' => 't_status',
    ];

    public $columns;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['columns',], 'required'],
            // ['columns', 'default', 'value' => array_values(self::COLUMNS_DEFAULT)],
            ['columns', 'each', 'rule' => [
                'in', 'range' => array_values(self::COLUMNS_DEFAULT)
            ]],
            ['columns', function ($attribute, $params) {
                if (!in_array('id', $this->$attribute)) {
                    $this->addError($attribute, 'id必须选中');
                }
            }],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'columns' => '要导出的字段',
        ];
    }

    public function updateToDefault()
    {
        $this->columns = array_values(self::COLUMNS_DEFAULT);
    }

    public function export() {
        if ($this->validate()) {
            return Supplier::find()
                ->select($this->columns)
                ->asArray()
                ->all();
        }
        return false;
    }
}
